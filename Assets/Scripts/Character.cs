﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public enum State
    {
        Idle,
        RunningToEnemy,
        RunningFromEnemy,
        BeginAttack,
        Attack,
        BeginShoot,
        Shoot,
        Death,
    }

    public enum Weapon
    {
        Pistol,
        Bat,
        Punch,
    }

    Animator animator;
    State state;

    public Weapon weapon;
    public Transform target;
    public float runSpeed;
    public float distanceFromEnemy;
    Vector3 originalPosition;
    Quaternion originalRotation;

    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        state = State.Idle;
        originalPosition = transform.position;
        originalRotation = transform.rotation;
    }

    public void SetState(State newState)
    {
        state = newState;
    }

    public State GetState()
    {
        return state;
    }

    [ContextMenu("Attack")]
    void AttackEnemy()
    {
        if (target.GetComponent<Character>().GetState() != State.Death)
        {
            switch (weapon) {
                case Weapon.Bat:
                    state = State.RunningToEnemy;
                    break;
                case Weapon.Punch:
                    state = State.RunningToEnemy;
                    break;
                case Weapon.Pistol:
                    state = State.BeginShoot;
                    break;
            }
            
        }
    }

    void FixedUpdate()
    {
        switch (state) {
            case State.Idle:
                transform.rotation = originalRotation;
                animator.SetFloat("Speed", 0.0f);
                break;

            case State.RunningToEnemy:
                animator.SetFloat("Speed", runSpeed);
                if (RunTowards(target.position, distanceFromEnemy))
                    state = State.BeginAttack;
                break;

            case State.RunningFromEnemy:
                animator.SetFloat("Speed", runSpeed);
                if (RunTowards(originalPosition, 0.0f))
                    state = State.Idle;
                break;

            case State.BeginAttack:
                if (weapon == Weapon.Bat)
                    animator.SetTrigger("MeleeAttack");
                if (weapon == Weapon.Punch)
                {
                    animator.SetTrigger("Punch");
                }
                state = State.Attack;
                break;

            case State.Attack:
                target.gameObject.GetComponentInChildren<Animator>().SetTrigger("Death");
                break;

            case State.BeginShoot:
                animator.SetTrigger("Shoot");
                state = State.Shoot;
                break;

            case State.Shoot:
                target.gameObject.GetComponentInChildren<Animator>().SetTrigger("Death");
                break;
            case State.Death:
                break;
        }
    }
    
    bool RunTowards(Vector3 targetPosition, float distanceFromTarget)
        {
            Vector3 distance = targetPosition - transform.position;
            if (distance.magnitude < 0.00001f) {
                transform.position = targetPosition;
                return true;
            }
    
            Vector3 direction = distance.normalized;
            transform.rotation = Quaternion.LookRotation(direction);
    
            targetPosition -= direction * distanceFromTarget;
            distance = (targetPosition - transform.position);
    
            Vector3 step = direction * runSpeed;
            if (step.magnitude < distance.magnitude) {
                transform.position += step;
                return false;
            }
    
            transform.position = targetPosition;
            return true;
        }
}
