﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationEvents : MonoBehaviour
{
    Character character;

    void Start()
    {
        character = GetComponentInParent<Character>();
    }

    void ShootEnd()
    {
        character.target.gameObject.GetComponentInChildren<Animator>().SetTrigger("Death");
        character.SetState(Character.State.Idle);
    }

    void AttackEnd()
    {
        character.target.gameObject.GetComponentInChildren<Animator>().SetTrigger("Death");
        character.SetState(Character.State.RunningFromEnemy);
    }

    void DeathEnd()
    {
        character.SetState(Character.State.Death);
    }
}
